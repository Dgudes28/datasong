import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { LRCFile, LRCFileReader, SongWithUrl, TimeStamp } from 'src/models/lrcData';
import * as stringSimilarity from 'string-similarity';
import { GameService } from '../core/game.service';

export enum GameStates {
  Playing = 'Playing',
  Success = 'Success',
  Failue = 'Failure'
}

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss'],
})

export class GameComponent implements OnInit {
  public isPlayingSong: boolean = true;
  public gameState: GameStates = GameStates.Playing;
  public nextLine: string;
  public pointsToAdd: number;
  public guessTime: number;
  public songsFiles: SongWithUrl[] =
    [<SongWithUrl>{ lrcFileName: "The-Weeknd-Save-Your-Tears-(Audio).txt", url: "https://storage.googleapis.com/data_song_bucket/The%20Weeknd%20-%20Save%20Your%20Tears%20(Official%20Music%20Video).mp3" },
    <SongWithUrl>{ lrcFileName: "Disney's-Frozen-Let-It-Go-Sequence-Performed-by-Idina-Menzel.txt", url: "https://storage.googleapis.com/data_song_bucket/Let%20It%20Go%20-%20Idina%20Menzel%20(from%20Disney's%20FROZEN)%20-%20Lyrics.mp3" }];
  // , <SongWithUrl>{ lrcFileName: "Taylor-Swift-Ready-For-It.txt", url: "https://storage.googleapis.com/data_song_bucket/Taylor%20Swift%20-%20...Ready%20For%20It%20(Lyrics).mp3" }];
  // https://storage.googleapis.com/data_song_bucket/FROZEN%20_%20Let%20It%20Go%20Sing-along%20_%20Official%20Disney%20UK.mp3  
  public songNumber: number;
  constructor(
    private httpClient: HttpClient,
    private gameService: GameService,
    private router: Router
  ) { }

  ngOnInit() {
    console.log('the random number is ' + this.songNumber);
    this.startSong();
  }

  startSong() {
    this.songNumber = Math.floor(Math.random() * this.songsFiles.length);
    let audio = new Audio(this.songsFiles[this.songNumber].url);
    let num = Math.floor(Math.random() * 10 + 10);
      audio.load();
    this.readLrcSong().subscribe((songObj: LRCFile) => {
      let startTimeInSeconds = this.calculateSeconds(songObj.songLine[num - 2].timeStamp);
      audio.currentTime = startTimeInSeconds;
      audio.play();
      setTimeout(() => {
        audio.pause();
        this.isPlayingSong = false;
        this.nextLine = songObj.songLine[num].text;
          this.guessTime = this.calculateMs(songObj.songLine[num + 1].timeStamp) - this.calculateMs(songObj.songLine[num].timeStamp);
        }, this.calculateMs(songObj.songLine[num].timeStamp) - startTimeInSeconds * 1000);
      });
    }

  readLrcSong(): Observable<LRCFile> {
    // this.httpClient.get('https://storage.googleapis.com/data_song_bucket/taylorSwiftReadyForIt.txt').
    //   subscribe(text => console.log(text),
    //     e => console.log(e)
    //   );
    let songFileName: string = this.songsFiles[this.songNumber].lrcFileName;
    // let songFileName = "Taylor-Swift-Ready-For-It.txt";
    return this.httpClient.get(`../../assets/${songFileName}`, {
      responseType: 'text'
    })
      .pipe(map(data => {
        console.log(data);
        let fileReader: LRCFileReader = new LRCFileReader();
        return fileReader.readFile(data);
      }));
  }

  calculateMs(time: TimeStamp): number {
    return (time.minute * 60 + time.seconds) * 1000 + time.ms;
  }

  calculateSeconds(time: TimeStamp): number {
    return time.minute * 60 + time.seconds;
  }

  continuePlaying(): void {
    if (this.gameService.lives === 0) {
      this.router.navigateByUrl('/gameover');
    }
    else {
      this.gameState = GameStates.Playing;
      this.isPlayingSong = true;
      this.startSong();
    }
  }

  compare(line: string): void {
    const similarity: number = stringSimilarity.compareTwoStrings(line, this.nextLine);

    if (similarity >= 0.5) {
      this.gameState = GameStates.Success;
      this.pointsToAdd = this.gameService.addToScore(similarity);
    }
    else {
      this.gameService.failure()
      this.gameState = GameStates.Failue;
    }
  }
}
