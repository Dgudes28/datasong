import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

const gifPaths: string[] = ['music', 'drums', 'claps', 'love'];

@Component({
  selector: 'app-success-page',
  templateUrl: './success-page.component.html',
  styleUrls: ['./success-page.component.scss'],
})
export class SuccessPageComponent implements OnInit {
  @Output()
  public continue: EventEmitter<void> = new EventEmitter<void>();
  @Input()
  public numberOfPointsToAdd: number;

  public path: string;

  constructor() { }

  ngOnInit() {
    this.path = gifPaths[Math.random() * gifPaths.length | 0];
  }
}
