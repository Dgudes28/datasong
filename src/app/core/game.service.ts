import { Injectable } from '@angular/core';

const DEFAULT_LIVES= 3;
const SCORE_MULTIPLIER = 100;

@Injectable({
  providedIn: 'root'
})
export class GameService {

  private _lives: number = DEFAULT_LIVES;
  public get lives(): number {
    return this._lives;
  }

  private _score: number = 0;
  public get score(): number {
    return this._score;
  }

  public addToScore(value: number): number {
    const scoreToAdd: number = Math.round(SCORE_MULTIPLIER * value);
    this._score +=  scoreToAdd;
    return scoreToAdd;
  }

  public reset(): void {
    this._lives = DEFAULT_LIVES;
    this._score = 0;
  }

  public failure(): number {
    if(this._lives > 0) {
      return this._lives--;
    }
  }

  constructor() { }
}
