import { Component, OnInit } from '@angular/core';
import { GameService } from '../core/game.service';

@Component({
  selector: 'app-game-over-page',
  templateUrl: './game-over-page.component.html',
  styleUrls: ['./game-over-page.component.scss'],
})
export class GameOverPageComponent implements OnInit {

  public get score(): number {
    return this.gameService.score;
  }
  constructor(private gameService: GameService) { }

  ngOnInit() {}

}
