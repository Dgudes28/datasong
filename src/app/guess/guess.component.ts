import { HttpClient } from '@angular/common/http';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import * as RecordRTC from 'recordrtc';

@Component({
  selector: 'app-guess',
  templateUrl: './guess.component.html',
  styleUrls: ['./guess.component.scss'],
})
export class GuessComponent implements OnInit {
  title = 'micRecorder';
  record;
  recording = false;
  url;
  error;
  @Input()
  public guessTime: number;
  @Output()
  public stoppedRecording: EventEmitter<string> = new EventEmitter<string>();

  constructor(private domSanitizer: DomSanitizer, private http: HttpClient) { }

  sanitize(url: string) {
    return this.domSanitizer.bypassSecurityTrustUrl(url);
  }
  /**
   * Start recording.
   */
  initiateRecording() {
    this.recording = true;
    let mediaConstraints = {
      video: false,
      audio: true,
    };
    navigator.mediaDevices
      .getUserMedia(mediaConstraints)
      .then(this.successCallback.bind(this), this.errorCallback.bind(this));
  }
  /**
   * Will be called automatically.
   */
  successCallback(stream: MediaStream) {
    var options = {
      mimeType: 'audio/wav',
      numberOfAudioChannels: 1
    };
    //Start Actuall Recording
    var StereoAudioRecorder = RecordRTC.StereoAudioRecorder;
    this.record = new StereoAudioRecorder(stream, options);
    this.record.record();
  }
  /**
   * Stop recording.
   */
  stopRecording() {
    this.recording = false;
    this.record.stop(this.processRecording.bind(this));
  }
  /**
   * processRecording Do what ever you want with blob
   * @param  {any} blob Blog
   */
  processRecording(blob) {
    this.url = URL.createObjectURL(blob);
    // var reader = new FileReader();
    // reader.readAsArrayBuffer(blob);
    // let data = reader.result;
    //don't need type informations
    // var mp3encoder = new lame.Mp3Encoder(1, 16000, 8);
    // var mp3Tmp = mp3encoder.encodeBuffer(blob.arrayBuffer, 0,
    //   Math.floor(blob.arrayBuffer
    //     .byteLength / 2));
    // debugger
    const formData = new FormData();
    formData.append('file', blob);

    this.http.post('http://localhost:3000/stt', formData).subscribe((line: string) => {
      this.stoppedRecording.emit(line);
    });
  }
  /**
   * Process Error.
   */
  errorCallback(error) {
    this.error = 'Can not play audio in your browser';
  }

  ngOnInit() {
    this.initiateRecording();
    setTimeout(() => {
      this.stopRecording();
    }, this.guessTime)
  }

}
