import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { interval } from 'rxjs';
import { take, finalize } from 'rxjs/operators'

@Component({
  selector: 'app-countdown',
  templateUrl: './countdown.component.html',
  styleUrls: ['./countdown.component.scss'],
})

export class CountdownComponent implements OnInit {
  @Input()
  public startNumber: number = 3;
  @Output()
  public finished: EventEmitter<void> = new EventEmitter<void>();
  public currentNumber: number;

  ngOnInit() {
    this.currentNumber = this.startNumber;
    interval(1000).pipe(take(this.currentNumber), finalize(() => this.finished.emit()))
      .subscribe(
        () => {
          this.currentNumber--;
        }
      );
  }

}
