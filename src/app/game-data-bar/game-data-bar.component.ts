import { Component, OnInit } from '@angular/core';
import { GameService } from '../core/game.service';

@Component({
  selector: 'app-game-data-bar',
  templateUrl: './game-data-bar.component.html',
  styleUrls: ['./game-data-bar.component.scss'],
})
export class GameDataBarComponent implements OnInit {

  public get lives(): number {
    return this.gameServcie.lives;
  }

  public get score(): number {
    return this.gameServcie.score;
  }

  constructor(private gameServcie: GameService) { }

  ngOnInit() {}

}
