import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

const gifPaths: string[] = ['bunny1', 'bunny2', 'bunny3', 'bunny4'];

@Component({
  selector: 'app-failure-page',
  templateUrl: './failure-page.component.html',
  styleUrls: ['./failure-page.component.scss'],
})
export class FailurePageComponent implements OnInit{
  public path: string;
  @Output()
  public continue: EventEmitter<void> = new EventEmitter<void>();
  @Input()
  public correctLine: string;

  ngOnInit() {
    this.path = gifPaths[Math.random() * gifPaths.length | 0];
  }
}
