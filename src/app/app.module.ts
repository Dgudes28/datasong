import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CountdownComponent } from './countdown/countdown.component';
import { GameDataBarComponent } from './game-data-bar/game-data-bar.component';
import { GuessComponent } from './guess/guess.component';
import { HomePageComponent } from './home-page/home-page.component';
import { SongPlayComponent } from './song-play/song-play.component';
import { GameComponent } from './game/game.component';
import { SuccessPageComponent } from './success-page/success-page.component';
import { FailurePageComponent } from './failure-page/failure-page.component';
import { GameOverPageComponent } from './game-over-page/game-over-page.component';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent, 
    CountdownComponent, 
    SongPlayComponent, 
    GuessComponent,
    GameDataBarComponent,
    GameComponent,
    SuccessPageComponent,
    FailurePageComponent,
    GameOverPageComponent
  ],
  entryComponents: [],
  imports: [BrowserModule, IonicModule.forRoot(), AppRoutingModule, HttpClientModule],
  providers: [{ provide: RouteReuseStrategy, useClass: IonicRouteStrategy }],
  bootstrap: [AppComponent],
})
export class AppModule { }
