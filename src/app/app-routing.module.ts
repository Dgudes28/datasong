import { Component, NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { FailurePageComponent } from './failure-page/failure-page.component';
import { GameOverPageComponent } from './game-over-page/game-over-page.component';
import { GameComponent } from './game/game.component';
import { HomePageComponent } from './home-page/home-page.component';
import { SuccessPageComponent } from './success-page/success-page.component';

const routes: Routes = [
  {
    path: 'home',
    component: HomePageComponent
  },
  {
    path: 'game',
    component: GameComponent
  },
  {
    path: 'success',
    component: SuccessPageComponent
  },
  {
    path: 'failure',
    component: FailurePageComponent
  },
  {
    path: 'gameover',
    component: GameOverPageComponent
  },
  {
    path: '**',
    component: HomePageComponent
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
