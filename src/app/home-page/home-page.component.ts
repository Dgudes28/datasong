import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GameService } from '../core/game.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent {

  public isLoading: boolean = false;
  
  constructor(private router: Router, private gameService: GameService) { }

  public startGame(): void {
    this.gameService.reset();
    this.router.navigate(['/game']);
  }

}
