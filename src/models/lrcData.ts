export class SongWithUrl {
    url: string;
    lrcFileName: string;
}

export class LRCFile {
    songLine: SongLine[];

    constructor() {
        this.songLine = [];
    }

    append(songLine: SongLine) {
        this.songLine.push(songLine);
    }
}
export class SongLine {
    text: string;
    timeStamp: TimeStamp;
}
export class TimeStamp {
    constructor(str: string) {
        let values = str.split(':');
        this.minute = parseInt(values[0]);
        this.seconds = parseInt(values[1].split('.')[0]);
        this.ms = parseInt(values[1].split('.')[1]);
    }
    minute: number;
    seconds: number;
    ms: number;
}
export class LRCFileReader {
    constructor() {
    }
    readFile(str: string): LRCFile {
        let text;
        let timeStamp: TimeStamp;
        let splittedLine: string[];
        let songLine: SongLine;
        let LrcFile: LRCFile = new LRCFile();
        let invalidValues = false;
        let lines = str.split('\n').map(x => x.replace('\t', '').replace('\r', '').replace('[', ''));
        lines.forEach(line => {
            splittedLine = line.split(']');
            text = splittedLine[1];
            try {
                timeStamp = new TimeStamp(splittedLine[0]);
            }
            catch {
                invalidValues = true;
            }
            songLine = <SongLine>{ text: text, timeStamp: timeStamp };
            if (songLine.text != '' && str != '' && invalidValues == false) {
                LrcFile.append(songLine);
            }
            else {
                invalidValues = false;
            }
        });
        return LrcFile;
    }
}