﻿[00:09.20]I saw you dancing in a crowded room
[00:13.20]You look so happy when I'm not with you
[00:17.30]But then you saw me, caught you by surprise
[00:21.40]A single teardrop falling from your eye
[00:26.30]I don't know why I run away
[00:34.40]I'll make you cry when I run away
[00:41.70]You could've asked me why I broke your heart
[00:46.00]You could've told me that you fell apart
[00:49.90]But you walked past me like I wasn't there
[00:53.80]And just pretended like you didn't care
[00:58.50]I don't know why I run away
[01:06.40]I'll make you cry when I run away
[01:14.70]Take me back 'cause I wanna stay
[01:18.70]Save your tears for another
[01:21.70]Save your tears for another day
[01:29.80]Save your tears for another day
[01:37.00]So, I made you think that I would always stay
[01:43.00]I said some things that I should never say
[01:46.70]Yeah, I broke your heart like someone did to mine
[01:50.70]And now you won't love me for a second time
[01:55.60]I don't know why I run away, oh, girl
[02:03.30]Said I make you cry when I run away
[02:11.50]Girl, take me back 'cause I wanna stay
[02:15.70]Save your tears for another
[02:19.30]I realize that I'm much too late
[02:23.70]And you deserve someone better
[02:26.90]Save your tears for another day (Ooh, yeah)
[02:34.90]Save your tears for another day (Yeah)
[02:46.10]I don't know why I run away
[02:52.70]I'll make you cry when I run away
[02:59.40]Save your tears for another day, ooh, girl (Ah)
[03:06.70]I said save your tears for another day (Ah)
[03:15.70]Save your tears for another day (Ah)
[03:23.70]Save your tears for another day (Ah)
[03:34.30]by RentAnAdviser.com
